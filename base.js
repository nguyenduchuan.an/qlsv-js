var Student = {
    viewStudent: () => { 
        let listStudent = JSON.parse(localStorage.getItem('qlsv'));
        let htmlInsert = "";
        if(listStudent){
            listStudent.map( (student, index) => {
                if(student){
                    htmlInsert += "<tr>"
                                    +"<td>"+ index +"</td>"
                                    +"<td>"+student.id +"</td>"
                                    +"<td>"+student.name +"</td>"
                                    +"<td>"+student.address +"</td>"
                                    +"<td>"
                                        +"<button type=\"button\" class=\"btn edit btn-warning\" data-id=\""+student.id+"\">Edit</button>"
                                        +"<button type=\"button\" class=\"btn del btn-danger\" data-id=\""+student.id+"\">Delete</button>"
                                    +"</td>"
                                +"</tr>";
    
                }
                document.getElementById('body').innerHTML = "";
                document.getElementById('body').innerHTML = htmlInsert;
            })
        }
    },
    addStudent : (id, name, address) => {
        var student = {
            id : id,
            name: name,
            address: address
        }
        var listStudent = JSON.parse(localStorage.getItem('qlsv'));
        if(!listStudent){
            listStudent = [];
        }
        let buttonType = document.getElementById("submit").getAttribute("type");
        if(buttonType === "add"){
            listStudent.push(student);
        }
        
        localStorage.setItem('qlsv', JSON.stringify(listStudent));;
    },
    editStudent: (idEdit, id, name, address) => {
        let item = {
            id : id,
            name : name,
            address : address
        };
        
        let buttonType = document.getElementById("submit").getAttribute("type");
        // Tìm sinh viên cần edit
        let data = JSON.parse(localStorage.getItem('qlsv'));
        if( data ){
            for(let i = 0; i < data.length; i++){
                if (data[i].id === idEdit) { // nếu là sinh viên cần edit
                    if(buttonType === "edit"){
                        data.splice(i, 1, item);
                    }
                }
            }
            localStorage.setItem('qlsv', JSON.stringify(data));
        }

    },
    delStudent: (id) => {
        let listStudent = JSON.parse(localStorage.getItem('qlsv'));
        if(!listStudent){
            listStudent =[];
        }
        if(listStudent){
            listStudent.map( (student, index) => {
                if(student.id === id){
                    listStudent.splice(index,1);
                }
            })
        }
        localStorage.setItem('qlsv', JSON.stringify(listStudent));;
    }
}
window.addEventListener('load', ()=>{
    Student.viewStudent();
    // Reset value  => add new
    var resetValue = () => {
        document.getElementById("msv").value = "";
            document.getElementById("name").value = "";
            document.getElementById("address").value = "";
       
    }
    document.getElementById('btn-add-new').addEventListener('click', () => {
        resetValue();
        console.log("object")
        document.getElementById("submit").setAttribute("type", "add");
        document.getElementById("add").style.display = "block";
    })
    document.getElementById('btn-clear').addEventListener('click', () => {
        resetValue();
    })

    // Event Click submit
    document.getElementById("submit").addEventListener("click", () => {
        let id = document.getElementById("msv").value;
        let name = document.getElementById("name").value;
        let address = document.getElementById("address").value;
        Student.addStudent(id, name, address);
        Student.viewStudent();
        document.getElementById("add").style.display = "none";
    });
    document.getElementById("body").addEventListener('click', function (e) {
        if(e.target.classList[1] === "del"){
            let idDel = e.target.getAttribute('data-id');
            Student.delStudent(idDel);
            Student.viewStudent();
        }else if(e.target.classList[1] === "edit"){
            document.getElementById("add").style.display = "block";
            document.getElementById("submit").setAttribute("type", "edit");
            let idEdit = e.target.getAttribute('data-id');
            console.log("object")
            let data = JSON.parse(localStorage.getItem('qlsv'));
            if( data ){
                for(let i = 0; i < data.length; i++){
                    if (data[i].id === idEdit) { // nếu là sinh viên cần sửa fill data vào form
                        document.getElementById("msv").value = data[i].id;
                        document.getElementById("name").value = data[i].name;
                        document.getElementById("address").value = data[i].address;
                    }
                }
            }
    
            document.getElementById('submit').addEventListener('click', () => {
                let id = document.getElementById("msv").value;
                let name = document.getElementById("name").value;
                let address = document.getElementById("address").value;
                Student.editStudent(idEdit, id, name, address);
                Student.viewStudent();
            });
        }       
    })    
}, false)
